<?php

namespace Articles\Seeders;

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \DB::table('menus')->insert($this->getData());
    }

    public function getData()
    {
        return [
            ['id'=>990,'name'=>'Статьи','position'=>'left','logo'=>'fal fa-list-alt','controller'=>'Admin\ArticlesController','url'=>'articles','parent'=>5, 'order'=>5],
        ];
    }
}
