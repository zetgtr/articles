<?php

namespace Articles\Seeders;

use Illuminate\Database\Seeder;

class ArticlesDatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            ArticlesSettings::class,
            MenuSeeder::class
        ]);
    }
}
