<?php

namespace Articles\Seeders;

use Illuminate\Database\Seeder;

class ArticlesSettings extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \DB::table('articles_settings')->insert($this->getData());
    }

    public function getData(): array
    {
        return [
            ['id'=>1,'url'=>'articles'],
        ];
    }
}
