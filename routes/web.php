<?php

use Illuminate\Support\Facades\Route;
use Articles\Http\Controllers\CategoryController;
use Articles\Http\Controllers\ArticlesController;
use Articles\Http\Controllers\SettingsController;
use Articles\Models\Settings;



Route::middleware('web')->group(function (){
    Route::middleware('auth_pacage')->group(function () {
        Route::group(['prefix'=>"admin", 'as'=>'admin.', 'middleware' => 'is_admin'],static function(){
            Route::resource('articles', ArticlesController::class);

            Route::group(['prefix' => 'articles', 'as' => 'articles.'], static function(){
                Route::post('remove_image/{articles}',[ArticlesController::class,'removeImage']);
                Route::resource('category', CategoryController::class);
                Route::resource('settings', SettingsController::class);
            });
        });
    });
    try {
        $settings = Settings::find(1);
        Route::get('/'.$settings->url,[ArticlesController::class,'frontIndex'])->name('articles');
        Route::get('/'.$settings->url . '/{url}',[ArticlesController::class,'articles'])->name('articles.articles');
    } catch (Exception $exception) {
        //throw $th;
    }
});
