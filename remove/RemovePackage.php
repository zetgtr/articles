<?php

namespace Articles\Remove;

use App\Models\Admin\Menu;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;

class RemovePackage
{
    private $pathMigration;
    private $pathScript;
    private $pathVues;
    public function __construct()
    {
        $this->pathMigration = database_path('migrations');
        $this->pathScript = public_path('assets/js/admin/articles');
        $this->pathVues = resource_path('views/vendor/articles');
    }

    public function run($settings,$migration = false, $script = false, $vies = false)
    {
        chdir(base_path());
        if($migration)
        {
            $categories = $this->pathMigration."/2023_03_05_203745_create_categories_articles_table.php";
            $articles = $this->pathMigration."/2023_03_07_181530_create_articles_table.php";
            $articlesHasCategories = $this->pathMigration."/2023_03_08_071047_create_categories_has_articles_table.php";
            $settingsPath = $this->pathMigration."/2023_03_09_145210_create_articles_settings_table.php";
            if (File::exists($settingsPath)) {
                Artisan::call('migrate:rollback', [
                    '--path' => 'database/migrations/2023_03_09_145210_create_articles_settings_table.php'
                    ]);
                unlink($settingsPath);
            }
            if (File::exists($articlesHasCategories)) {
                Artisan::call('migrate:rollback', [
                    '--path' => 'database/migrations/2023_03_08_071047_create_categories_has_articles_table.php'
                ]);
                unlink($articlesHasCategories);

            }
            if (File::exists($categories)) {
                Artisan::call('migrate:rollback', [
                    '--path' => 'database/migrations/2023_03_05_203745_create_categories_articles_table.php'
                ]);
                unlink($categories);
            }
            if (File::exists($articles)) {
                Artisan::call('migrate:rollback', [
                    '--path' => 'database/migrations/2023_03_07_181530_create_articles_table.php'
                ]);
                unlink($articles);
            }


            $menu = Menu::query()->find(990);
            if($menu)
                $menu->delete();
        }

        if($script)
        {
            if (File::isDirectory($this->pathScript))
                File::deleteDirectory($this->pathScript);
        }

        if ($vies)
        {
            if (File::isDirectory($this->pathVues))
                File::deleteDirectory($this->pathVues);
        }
    }
}
