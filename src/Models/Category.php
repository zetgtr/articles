<?php

namespace Articles\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $table = 'categories_articles';

    protected $fillable = [
        'url','name'
    ];
}

