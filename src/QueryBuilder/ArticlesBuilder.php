<?php

namespace Articles\QueryBuilder;

use Illuminate\Database\Eloquent\Collection;
use Articles\Enums\ArticlesEnums;
use Articles\Models\Category;
use Articles\Models\Article;

class ArticlesBuilder extends QueryBuilder
{
    public function __construct()
    {
        $this->model = Article::query();
        
    }

    public function getLinksContent($key)
    {
        $links = [ArticlesEnums::CONTENT->value => ['url'=> ArticlesEnums::CONTENT->value, 'name' => 'Контент'],
            ArticlesEnums::SEO->value => ['url'=> ArticlesEnums::SEO->value,  'name' => 'SEO']
        ];

        $links[$key]['active'] = true;

        return $links;
    }
    public function getLinks($key)
    {
        $links = [
            ArticlesEnums::ARTICLES->value => ['url'=> route('admin.articles.index'), 'name' => 'Статьи'],
            ArticlesEnums::POST->value => ['url'=> route('admin.articles.create'),  'name' => 'Пост'],
            ArticlesEnums::CATEGORY->value => ['url'=> route('admin.articles.category.create'),  'name' => 'Категории'],
            ArticlesEnums::SETTINGS->value => ['url'=> route('admin.articles.settings.create'),  'name' => 'Настройки']
        ];

        if($key) $links[$key]['active'] = true;

        return $links;
    }

    public function getAll(): Collection
    {
        return $this->model->orderBy('created_at', 'desc')->get();
    }
}
