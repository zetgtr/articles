<?php

namespace Articles\Http\Controllers;

use Articles\Enums\ArticlesEnums;
use Articles\Models\Category;
use Articles\QueryBuilder\ArticlesBuilder;
use Articles\Requests\CategoryRequest;

class CategoryController
{
    /**
     * Display a listing of the resource.
     */
    public function index(ArticlesBuilder $articlesBuilder)
    {

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(ArticlesBuilder $articlesBuilder)
    {
        return view('articles::articles.category',[
            'links' => $articlesBuilder->getLinks(ArticlesEnums::CATEGORY->value)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CategoryRequest $request, ArticlesBuilder $articlesBuilder)
    {
        $category = Category::create($request->validated());
        if ($category) {
            return \redirect()->route('admin.articles.category.create')->with('success', "Успешно создано");
        }

        return \back()->with('error', "Ошибка создания");
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Category $category)
    {
        if(!$category)
        {
            return ['status'=>false, 'message'=>'Ошибка. Попробуйте позже'];
        }
        return ['status'=>true,'category'=>$category];
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CategoryRequest $request, Category $category, ArticlesBuilder $articlesBuilder)
    {
        $category = $category->fill($request->validated());
        if ($category->save()) {
            return \redirect()->route('admin.articles.category.create')->with('success', "Успешно обновлено");
        }

        return \back()->with('error', "Ошибка обновления");

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Category $category)
    {
        try {
            $category->delete();
            $response = ['status' => true,'message' => __('messages.admin.articles.destroy.success')];
        } catch (\Exception $exception)
        {
            $response = ['status' => false,'message' => __('messages.admin.articles.destroy.fail') . $exception->getMessage()];
        }

        return $response;
    }
}
