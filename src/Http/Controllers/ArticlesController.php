<?php

namespace Articles\Http\Controllers;


use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;
use Articles\Enums\ArticlesEnums;
use Articles\Models\Article;
use Articles\QueryBuilder\ArticlesBuilder;
use Articles\Requests\CreateRequest;
use Articles\Requests\UpdateRequest;
use Illuminate\Contracts\Validation\Factory;
use Articles\Models\Settings;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(ArticlesBuilder $articlesBuilder,Request $request)
    {
        return view('articles::articles.index',[
            'links' => $articlesBuilder->getLinks(ArticlesEnums::ARTICLES->value)
        ]);
    }

    public function removeImage(Article $article,Request $request)
    {

        try {
            $images = $article->images;
            $filteredImages = array_filter($images, function ($image) use ($request) {
                return $image !== $request->input('img');
            });
            $article->images = json_encode(array_values($filteredImages));
            if($article->save()){
                return ['status'=>true];
            }
        }catch (Exception $exception){
            return ['status'=>false];
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(ArticlesBuilder $articlesBuilder,Factory $factory)
    {
        return view('articles::articles.create',[
            'linksContent' => $articlesBuilder->getLinksContent(ArticlesEnums::CONTENT->value),
            'links' => $articlesBuilder->getLinks(ArticlesEnums::POST->value),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateRequest $request
     * @return RedirectResponse
     */
    public function store(CreateRequest $request): RedirectResponse
    {
        $article = Article::create($request->validated());
        if ($article) {
            $article->categories()->attach($request->getCategoriesIds());
            return \redirect()->route('admin.articles.create')->with('success', "Успешно создано");
        }

        return \redirect()->route('admin.articles.create')->with('error', "Ошибка создания");
    }

    /**
     * Display the specified resource.
     */
    public function show(Article $article)
    {
        $article->publish = !$article->publish;
        if ($article->save()) return ['status' => true, 'publish' => $article->publish];
        else  return ['status' => false];
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Article $article, ArticlesBuilder $articlesBuilder)
    {

        return view('articles::articles.edit',[
            'linksContent' => $articlesBuilder->getLinksContent(ArticlesEnums::CONTENT->value),
            'links' => $articlesBuilder->getLinks(null),
            'article' => $article
        ]);
    }

    /**
     * Handle the incoming request.
     */
    public function frontIndex()
    {
        
        return \view('articles::articles.front.index',[
            'articles'=>Article::orderBy('created_at', 'desc')->paginate(Settings::first()->paginate),
            'settings'=> Settings::query()->find(1),
]);
    }
    public function articles(string $url)
    {
        return view('articles::articles.front.articles',['article'=>Article::where('url',$url)->first(),'settings'=> Settings::query()->find(1)]);
    }
    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, Article $article)
    {
        $article = $article->fill($request->validated());
        if ($article->save()) {
            $article->categories()->sync($request->getCategoriesIds());
            return \redirect()->route('admin.articles.edit',['article'=>$article])->with('success', "Успешно обновлено");
        }

        return \back()->with('error', "Ошибка обновления");
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Article $article)
    {
        try {
            $article->delete();
            $response = ['status' => true,'message' => __('messages.admin.articles.destroy.success')];
        } catch (Exception $exception)
        {
            $response = ['status' => false,'message' => __('messages.admin.articles.destroy.fail') . $exception->getMessage()];
        }

        return $response;
    }
}
