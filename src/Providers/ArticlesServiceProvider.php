<?php

namespace Articles\Providers;

use App\Actions\Fortify\CreateNewUser;
use App\Actions\Fortify\ResetUserPassword;
use App\Actions\Fortify\UpdateUserPassword;
use App\Actions\Fortify\UpdateUserProfileInformation;
use App\QueryBuilder\RolesBuilder;
use Articles\View\Articles;
use Illuminate\Support\ServiceProvider;
use Laravel\Fortify\Fortify;
use Articles\QueryBuilder\CategoryBuilder;
use Articles\QueryBuilder\ArticlesBuilder;
use Articles\QueryBuilder\QueryBuilder;
use Articles\Seeders\ArticlesDatabaseSeeder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\AliasLoader;
use Articles\Http\Middleware\Auth as PacageAuth;
use Articles\View\Category;
use Articles\View\Content;
use Articles\View\EditContent;
use Articles\View\Article;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Articles\View\Settings;
use Articles\Models\Article as ArticleModel;

class ArticlesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any package services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');

        }
        $this->app['router']->aliasMiddleware('auth_pacage', PacageAuth::class);

        $this->publishes([
            __DIR__ . '/../../database/migrations' => database_path('migrations'),
        ], 'migrations');
        $this->publishes([
            __DIR__.'/../../config/articles.php' => config_path('articles.php'),
        ], 'config_articles');

        $this->publishes([
            __DIR__.'/../../resources/views' => resource_path('views/vendor/articles'),
        ], 'views');
        $this->publishes([
            __DIR__.'/../../resources/js' => public_path('assets/js/admin/articles'),
        ], 'script');
        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'articles');
        $this->loadViewsFrom(__DIR__.'/../../resources/components', 'articles');
        $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
        $this->components();
        $this->app->bind('articles', function ($app) {
            return $app->make(ArticlesModel::class);
        });
    }

    private function components()
    {
        Blade::component(Articles::class, 'articles::articles');
        Blade::component(EditContent::class, 'articles::edit-content');
        Blade::component(Content::class, 'articles::content');
        Blade::component(Category::class, 'articles::category');
        Blade::component(Settings::class, 'articles::settings');
    }

    private function singletons()
    {
        $this->app->singleton(Articles::class, function ($app) {
            $articlesBuilder = $app->make(ArticlesBuilder::class);
            return new Articles($articlesBuilder);
        });
        $this->app->singleton(EditContent::class, function ($app, $parameters) {
            $categoryBuilder = $app->make(CategoryBuilder::class);
            $rolesBuilder = $app->make(RolesBuilder::class);
            $articleItem = $parameters['articleItem'];
            
            return new EditContent($categoryBuilder, $articleItem, $rolesBuilder);
        });
        $this->app->singleton(Content::class, function ($app) {
            $categoryBuilder = $app->make(CategoryBuilder::class);
            $rolesBuilder = $app->make(RolesBuilder::class);
            return new Content($categoryBuilder,$rolesBuilder);
        });
        $this->app->singleton(Category::class, function ($app) {
            $categoryBuilder = $app->make(CategoryBuilder::class);
            return new Category($categoryBuilder);
        });
        $this->app->singleton(Settings::class, function ($app) {
            return new Settings();
        });
    }
    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(QueryBuilder::class, ArticlesBuilder::class);
        $this->app->bind(QueryBuilder::class, CategoryBuilder::class);
        $this->mergeConfigFrom(__DIR__.'/../../config/articles.php', 'articles');
        $this->singletons();
    }
}
