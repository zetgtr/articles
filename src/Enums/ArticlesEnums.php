<?php

namespace Articles\Enums;

enum ArticlesEnums: string
{
    case CONTENT = "content";
    case PHOTO = "photo";
    case SEO = "seo";

    case ARTICLES = "articles";
    case POST = "post";
    case CATEGORY = "category_articles";

    case EDIT = "edit";
    case SETTINGS = "settings";
}

