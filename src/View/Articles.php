<?php

namespace Articles\View;

use Articles\QueryBuilder\ArticlesBuilder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Date;
use Illuminate\View\Component;
use Articles\QueryBuilder\Article;

class Articles extends Component
{
    private Collection $articles;
    /**
     * Create a new component instance.
     */
    public function __construct(ArticlesBuilder $articlesBuilder)
    {
        $this->articles = $articlesBuilder->getAll();
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render()
    {
        return view('articles::components.articles',['articlesList'=>$this->articles]);
    }
}
