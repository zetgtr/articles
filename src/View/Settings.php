<?php

namespace Articles\View;


use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;
use Articles\Models\Settings as ArticlesSettings;

class Settings extends Component
{
    private ArticlesSettings $settings;
    /**
     * Create a new component instance.
     */
    public function __construct()
    {
        $this->settings = ArticlesSettings::query()->find(1);
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('articles::components.settings',['settings'=>$this->settings]);
    }
}
