<?php

namespace Articles\View;

use App\QueryBuilder\RolesBuilder;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\Component;
use Articles\QueryBuilder\CategoryBuilder;
use Articles\Models\Article;

class EditContent extends Component
{
    private Collection $categories;
    private Article $article;
    /**
     * Create a new component instance.
     */
    public function __construct(CategoryBuilder $categoryBuilder,Article $articleItem, RolesBuilder $rolesBuilder)
    {
        $this->categories = $categoryBuilder->getAll();
        $this->article = $articleItem;
        $this->roles = $rolesBuilder->getAll();
    }


    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        if(count($this->article->categories()->pluck('id')))
            $categoryArticles = $this->article->categories()->pluck('id')[0];
        else $categoryArticles = 1;
        return view('articles::components.edit-content',['categories' => $this->categories,'article' => $this->article, 'categoryArticles'=>$categoryArticles, 'roles' => $this->roles]);
    }
}
