{{-- @dd($news) --}}
@extends('layouts.inner')
@section('title',$article->seoTitle)
@section('description',$article->seoDescription)
@section('keywords',$article->seoKeywords)
@section('page',true)
@section('content')
<div class="content">

    <x-articles::front.item :article="$article" />
</div>

@endsection
