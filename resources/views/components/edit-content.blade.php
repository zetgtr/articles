<style>
    .img_box{
        position: relative;
    }
    .remove{
        position: absolute;
        right: 15px;
        cursor: pointer;
        font-size: 16px;
    }
    .remove:hover{
        color: red;
    }
</style>
<div class="tab-pane active" id="content" role="tabpanel">
    <input type="hidden" name="id" value="{{ $article->id }}" />
    <div class="row">
        <div class="col-lg-7">
            <div class="form-grop mb-3">
                <label for="title">Заголовок</label>
                <input id="title" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') ? old('title') : $article->title }}" />
                <x-error error-value="title" />
            </div>
            <div class="form-group mb-3">
                <label for="my-editor" >Описание</label>
                <textarea name="description" id="my-editor" class="form-control @error('description') is-invalid @enderror my-editor">{{ old('description') ? old('description') : $article->description }}</textarea>
                <x-error error-value="description" />
            </div>
            <div class="form-group mb-3">
                <label for="content" >Контент</label>
                <textarea name="content" id="my-editor" class="form-control @error('content') is-invalid @enderror my-editor">{{ old('content') ? old('content') : $article->content }}</textarea>
                <x-error error-value="content" />
            </div>
        </div>
        <div class="col-lg-5">
            <div class="form-group">
                <label for="category">Категория</label>
                <select name="category_id" id="category" class="form-select">
                    @foreach($categories as $category)
                        <option @if($categoryArticles === $category->id) selected @endif value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="access">Доступ</label>
                <select name="access" class="form-control @error('access') is-invalid @enderror form-select">
                    <option value="0" selected="selected">Общий</option>
                    @foreach($roles as $role)
                        <option @selected(old('access') === $role->id || $article->access === $role->id ) value="{{ $article->id }}">{{ $role->name }}</option>
                    @endforeach
                </select>
                <x-error error-value="access" />
            </div>
            <div class="form-group" style="position:relative;">
                <label for="date_articles">Дата публикации (необязательно):</label>
                <input type="text" data-language="ru" name="created_at" id="addDates" class="form-control @error('created_at') is-invalid @enderror" value="{{ old('created_at') ? old('created_at') : $article->created_at->format('d.m.Y H:i') }}">
            </div>
            <div class="form-group ">
                <label for="">Изображения</label>
                <input type="file" class="filepond " name="img[]" multiple>
                <x-error error-value="img" />
                <div class="pb-0 mt-3">
                    <ul id="lightgallery" class="list-unstyled row">
                        @foreach($article->images as $image)
                            @if($image !== "")
                                <li class="col-xs-6 col-sm-4 col-md-4 col-xl-4 img_box mb-5 border-bottom-0"
                                    data-responsive="{{$image}}"
                                    data-src="{{$image}}"
                                    data-articles="{{ $article->id }}"
                                >
                                    <div class="remove"><span class="glyphicon glyphicon-remove"></span></div>
                                    <a href="javascript:void(0)">
                                        <img class="img-responsive br-5" src="{{$image}}" alt="Thumb-1">
                                    </a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="{{ asset('assets/plugins/gallery/lightgallery.js') }}"></script>
<script src="{{ asset('assets/plugins/gallery/lightgallery-1.js') }}"></script>
<script>
    $(document).ready(()=>{
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        document.querySelectorAll('.remove').forEach(el=>{
            el.addEventListener('click',(e)=>{
                e.preventDefault()
                setTimeout(()=>{
                    document.querySelector('.lg-outer').remove();
                    document.querySelector('.lg-backdrop').remove();
                },0)
                const contaierImg = e.target.closest('.img_box');
                const img = contaierImg.dataset.src
                const articles = contaierImg.dataset.articles
                $.ajax({
                    type: "POST",
                    url: '/admin/articles/remove_image/'+articles,
                    data: {
                        img
                    },
                    success(request){
                        if(request.status)
                            contaierImg.remove()
                    }
                })
            })
        })
    })
</script>

@vite('resources/js/utils/AirDatepicker.js')
