<div class="card-header card-header-divider">
    <div>
        <h4>Список статей</h4>
    </div>
</div>
<div class="card-body">
    <div class="table-responsive">
        <table id="example2" class="table table-bordered text-nowrap border-bottom">
            <thead>
            <tr>
                <th class="border-bottom-0 text-center">Заголовок</th>
                <th class="border-bottom-0 text-center">Дата создания</th>
                <th class="border-bottom-0 text-center">Просмотры</th>
                <th class="border-bottom-0 text-center">Действие</th>
            </tr>
            </thead>
            <tbody>
            @foreach($articlesList as $articles)
                <tr class="delete-element">
                    <td>{{ $articles->title }}</td>
                    <td class="text-center">{{ $articles->created_at->format('d.m.Y H:i') }}</td>
                    <td class="text-center">{{ $articles->show }}</td>
                    <td class="text-center btn-list-table">
                        <a href="{{ route('admin.articles.edit', $articles) }}" class="btn btn-secondary">
                            <i class="fal fa-pencil-alt"></i>
                        </a>
                        <a href="{{ route('admin.articles.destroy', $articles) }}" class="btn btn-danger delete">
                            <i class="far fa-trash-alt"></i>
                        </a>
                        @if($articles->publish)
                            <a href="{{ route('admin.articles.show', $articles) }}" class="btn btn-success show-publish">
                                <i class="far fa-eye"></i>
                            </a>
                        @else
                            <a href="{{ route('admin.articles.show', $articles) }}" class="btn btn-default show-publish">
                                <i class="far fa-eye-slash"></i>
                            </a>
                        @endif

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

<script src="{{ asset('assets/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/js/dataTables.bootstrap5.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('assets/plugins/datatable/dataTables.responsive.min.js')}}"></script>
<script>
    $('#example2').DataTable({
        responsive: true,
        ordering: false,
        language: {
            searchPlaceholder: 'Поиск...',
            sSearch: '',
            lengthMenu: '_MENU_ Элементы на странице',
        }
    });
</script>
<script src="{{ asset('assets/js/admin/delete.js') }}"></script>
<script src="{{ asset('assets/js/admin/show.js') }}"></script>
