<div class="card-header card-header-divider">
    <div>
        <h4>Создание статьи</h4>
        <span class="card-header-subtitle">Заполните необходимые поля и сохраните статью.</span>
    </div>
</div>
<x-admin.navigatin-js :links="$links" />
<div class="card-body">
    <form action="{{ route('admin.articles.store') }}" method="POST" enctype="multipart/form-data" class="tab-content">
        @csrf
        @if ($errors->any())
            @foreach($errors->all() as $error)
                <x-alert type="danger" :message="$error"></x-alert>
            @endforeach
        @endif
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <x-articles::content />
        <x-articles::seo />
        <button type="submit" name="save" class="btn btn-sm btn-success">Сохранить</button>
    </form>
</div>

