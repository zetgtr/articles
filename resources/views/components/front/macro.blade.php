@php
use Carbon\Carbon;
$formattedDate = Carbon::parse($item->created_at)->locale('ru')->isoFormat('D MMMM YYYY');
@endphp
<div class=" articles-item">
    @if($item->images)
    <div class="articles-item__image">
        <a class="image image_alive" href="{{ route('articles.articles', ['url'=>$item->url]) }}">
            <img src="{{ $item->images[0] }}" alt="">
        </a>
    </div>
    @endif
    <a href="{{ route('articles.articles', ['url'=>$item->url]) }}" class="articles-item__upkeep">
        <div class="articles-item__header">
            <div class="articles-item__date">
                {{ $formattedDate }}
            </div>
            @if(empty($flag))
            <div class="articles-item__info">
                <div class="articles-item__eye">
                    <i class="far fa-eye"></i>
                    {{ $item->show }}
                </div>
            </div>
            @endif
        </div>
        <div class="articles-item__title">
            <span>{!! htmlspecialchars_decode(e($item->title)) !!}</span>
        </div>
        @if(empty($flag))
        <div class="articles-item__content">{!! $item->description !!}</div>
        @endif
    </a>
</div>
