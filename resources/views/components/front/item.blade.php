{{-- @dd($news) --}}
@php
use Carbon\Carbon;
$formattedDate = Carbon::parse($article->created_at)->locale('ru')->isoFormat('D MMMM YYYY');

$article->setCountShow();
@endphp
<div class="articles__category">
    <div class="container articles-container">
        <x-front.breadcrumbs :breadcrumbs="$article->getBredcrambs()" flag='true' />
        <h1 style="color: #000;">{!! $article->title !!}</h1>
        <div class="main">
            <div class="articles__header__item text-small text-gloom position-relative">
                <div class="articles__header-item">
                    <i class="far fa-clock"></i>
                    {{ $formattedDate }}
                </div>
                <div class="articles-item__info">
                    <div class="articles-item__eye">
                        <i class="far fa-eye"></i>
                        {{ $article->show }}
                    </div>
                </div>
                @auth
                <a target="_blank" class="articles-item__edit" href="{{ route("admin.articles.edit",  $article) }}">
                    <i class="far fa-edit"></i>
                    <span>Редактировать</span>
                </a>
                @endauth
            </div>

            <div class="articles__content articles-text">
                {!! $article->content !!}
                @if(($article->images) !== "[null]")
                <div class="articles-slider-container">
                    <div class="articles-slider swiper">
                        <div class="swiper-wrapper">
                            @foreach($article->images as $image)
                            <div class="swiper-slide">
                                <div class="item">
                                    <div class="img-container">
                                        <img src="{{ $image }}" alt="Slide Image">
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
                @endif
                @if($article->getOther())
                <div class="other-articles">
                    <div class="other-articles__header">
                        <h2>Другие статьи</h2>
                        <div class="other-articles__btns">
                            <div class="other__prev">
                                <svg width="90" height="52" viewBox="0 0 90 52" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M29.4 0.457546C28.9 0.457546 28.4 0.657546 28 0.957546L0.7 24.3575C0.3 24.7575 0 25.3575 0 25.9575C0 26.5575 0.3 27.1575 0.7 27.5575L28 50.9575C28.9 51.7575 30.2 51.6575 31 50.7575C31.8 49.8575 31.6 48.5575 30.8 47.7575L5.4 25.9575L30.8 4.15754C31.7 3.35754 31.8 2.05754 31 1.15754C30.6 0.757543 30 0.457546 29.4 0.457546Z" fill="#231F20"></path>
                                    <path d="M87.9 23.8575H2.1C0.9 23.8575 0 24.8575 0 25.9575C0 27.1575 0.9 28.0575 2.1 28.0575H87.9C89.1 28.0575 90 27.0575 90 25.9575C90 24.7575 89.1 23.8575 87.9 23.8575Z" fill="#231F20"></path>
                                </svg>
                            </div>
                            <div class="other__next">
                                <svg width="90" height="52" viewBox="0 0 90 52" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M60.6 0.457546C61.1 0.457546 61.6 0.657546 62 0.957546L89.3 24.3575C89.7 24.7575 90 25.3575 90 25.9575C90 26.5575 89.7 27.1575 89.3 27.5575L62 50.9575C61.1 51.7575 59.8 51.6575 59 50.7575C58.2 49.8575 58.4 48.5575 59.2 47.7575L84.6 25.9575L59.2 4.15754C58.3 3.35754 58.2 2.05754 59 1.15754C59.4 0.757543 60 0.457546 60.6 0.457546Z" fill="#231F20"></path>
                                    <path d="M2.1 23.8575H87.9C89.1 23.8575 90 24.8575 90 25.9575C90 27.1575 89.1 28.0575 87.9 28.0575H2.1C0.899998 28.0575 0 27.0575 0 25.9575C0 24.7575 0.899998 23.8575 2.1 23.8575Z" fill="#231F20"></path>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div class="swiper other-slider">
                        <div class="swiper-wrapper">
                            @foreach ($article->getOther() as $item)
                            <div class="swiper-slide">
                                <div class="item">
                                    <x-articles::front.macro :item="$item" flag='true' />
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                @endif


                <div class="articles__footer-item">
                    <a href="{{ route('articles') }}" class="articles-footer-item-back">
                        <i class="fas fa-caret-left"></i>
                        <span>Назад</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

@vite('resources/js/articles/slider.js')
