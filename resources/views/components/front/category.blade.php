@php
    $breadcrumbs = [...config('articles.routes')];
@endphp
<div class="articles__category">
    <div class="container">
        <x-front.breadcrumbs :breadcrumbs="$breadcrumbs"  flag='true'/>
        <h1>{{ $settings->title }}</h1>
        <div class="row articles-item--row">
            @foreach ( $articles as $item)
                <div class="col-lg-4 col-sm-6 wow fadeIn">
                    <x-articles::front.macro :item="$item" />
                </div>
            @endforeach
            <div>
                {{ $articles->links() }}
            </div>
        </div>
    </div>
</div>
